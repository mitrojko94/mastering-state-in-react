// App.js
import React, { useState, useEffect } from "react";
import "./index.css";

const App = () => {
  const [communityData, setCommunityData] = useState([]);
  const [email, setEmail] = useState("");
  const [subscribeLoading, setSubscribeLoading] = useState(false);
  const [unsubscribeLoading, setUnsubscribeLoading] = useState(false);
  const [error, setError] = useState(null);
  const [sectionVisible, setSectionVisible] = useState(true);

  useEffect(() => {
    fetchCommunityData();
  }, []);

  const fetchCommunityData = async () => {
    try {
      const response = await fetch("http://localhost:8000/community");
      const data = await response.json();
      setCommunityData(data);
    } catch (error) {
      console.error("Error fetching community data:", error);
    }
  };
  // SUBSCRIBE
  const handleSubscribe = async () => {
    try {
      setSubscribeLoading(true);
      if (email.toLowerCase() === "forbidden@gmail.com") {
        alert("Email is already in use");
        setEmail("");
      }

      // Make POST request to /subscribe
      const response = await fetch("http://localhost:8000/subscribe", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ email }),
      });

      // Handle server response
      if (response.ok) {
        // Subscription successful
        setSectionVisible(false); // Hide section on successful subscription
        setEmail(""); // Clear email input
      } else {
        const errorData = await response.json();
        setError(errorData.error.message);
        alert(errorData.error.message); // Show error message in an alert
      }
    } catch (error) {
      console.error("Error subscribing:", error);
    } finally {
      setSubscribeLoading(false);
    }
  };

  // UNSUBSCRIBE
  const handleUnsubscribe = async () => {
    try {
      setUnsubscribeLoading(true);

      // Make POST request to /unsubscribe
      const response = await fetch("http://localhost:8000/unsubscribe", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ email }),
      });

      // Handle server response
      if (response.ok) {
        // Unsubscription successful
        setSectionVisible(false); // Hide section on successful unsubscription
        setEmail("");
      } else {
        const errorData = await response.json();
        setError(errorData.error.message);
        alert(errorData.error.message); // Show error message in an alert
      }
    } catch (error) {
      console.error("Error unsubscribing:", error);
    } finally {
      setUnsubscribeLoading(false);
    }
  };

  const toggleSectionVisibility = () => {
    setSectionVisible(!sectionVisible);
  };

  return (
    <div>
      <section>
        <div className="community">
          <h2>
            Big Community of <br /> People Like You
          </h2>

          <button type="button" onClick={toggleSectionVisibility}>
            {sectionVisible ? "Hide section" : "Show section"}
          </button>
        </div>

        <h2 className="title">
          We're proud of our products, and we're really excited <br /> when we
          get feedback from our users.
        </h2>

        <div className="container">
          {communityData.map((person) => {
            return (
              <div key={person.id} className="personData">
                <img
                  src={person.avatar}
                  alt={person.firstName}
                  className="pImage"
                />
                <p className="pData">
                  Lorem ipsum dolor sit amet, <br />
                  consectetur adipiscing elit, sed do <br />
                  eiusmod tempor incididunt ut <br />
                  labore et dolor.
                </p>
                <h1 className="pName">
                  {person.firstName} {person.lastName}
                </h1>
                <p className="pPosition">{person.position}</p>
              </div>
            );
          })}
        </div>
      </section>

      <section style={{ opacity: sectionVisible ? 1 : 0 }}>
        <form className="form">
          <h2>Join Our Program</h2>
          <input
            type="email"
            placeholder="Enter your email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
          <button
            type="button"
            onClick={handleSubscribe}
            disabled={subscribeLoading || unsubscribeLoading}
            style={{
              opacity: subscribeLoading || unsubscribeLoading ? 0.5 : 1,
            }}
          >
            Subscribe
          </button>
          <button
            type="button"
            onClick={handleUnsubscribe}
            disabled={subscribeLoading || unsubscribeLoading}
            style={{
              opacity: subscribeLoading || unsubscribeLoading ? 0.5 : 1,
            }}
          >
            Unsubscribe
          </button>
        </form>
      </section>
    </div>
  );
};

export default App;
